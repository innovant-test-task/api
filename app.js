require("dotenv").config();

const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const authRoutes = require("./routes/auth-routes");
const app = express();

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE");
  next();
});

app.use("/api/auth", authRoutes);
app.use((req, res, next) => {
  const error = new Error("could not found this route.");
  error.code = 404;
  throw error;
});

app.use((error, req, res, next) => {
  if (req.headerSent) return next(error);
  res.status(error.code || 500);
  res.json({ message: error.message || "An unknow error occured!" });
});

mongoose
  .connect(
    `mongodb+srv://${process.env.MONGODB_USER}:${process.env.MONGODB_PASSWORD}@cluster0-e6ejd.mongodb.net/testTaskApp?retryWrites=true&w=majority`,
    { useUnifiedTopology: true, useNewUrlParser: true }
  )
  .then(() => {
    app.listen(5000);
    console.log(`Listening on port ${process.env.PORT}`);
  })
  .catch((err) => console.log(err));
