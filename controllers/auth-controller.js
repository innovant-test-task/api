const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const User = require("../models/user");

const signin = async (req, res, next) => {
  const { email, password } = req.body;

  let user;
  try {
    user = await User.findOne({ email: email });
  } catch (error) {
    const err = new Error("Somthing went wrong. could not login!");
    err.code = 500;
    return next(err);
  }

  if (!user) {
    const err = new Error("Invalid credentials provided, could not login.");
    err.code = 404;
    return next(err);
  }

  let isValidPass = false;
  try {
    isValidPass = await bcrypt.compare(password, user.password);
  } catch (error) {
    const err = new Error(
      "could not login user, please check your credentiels."
    );
    err.code = 500;
    return next(err);
  }

  if (!isValidPass) {
    const err = new Error("Invalid credentials provided, could not login.");
    err.code = 401;
    return next(err);
  }

  let token;
  try {
    token = jwt.sign(
      { userId: user.id, email: user.email },
      `${process.env.JWT_SECRET}`,
      { expiresIn: "1h" }
    );
  } catch (err) {
    const error = new Error("logging user failed. Please try again!");
    error.code = 500;
    return next(error);
  }

  res.json({
    userId: user.id,
    email: user.email,
    firstname: user.firstname,
    lastname: user.lastname,
    token: token,
  });
};

const register = async (req, res, next) => {
  const { email, password, firstName, lastName } = req.body;

  let addedUser;
  try {
    addedUser = await User.findOne({ email: email });
  } catch (error) {
    const err = new Error("Somthing went wrong. could not add user!");
    err.code = 500;
    return next(err);
  }

  if (addedUser) {
    const err = new Error("User already exists, please login instead.");
    err.code = 404;
    return next(err);
  }

  let hashPass;
  try {
    hashPass = await bcrypt.hash(password, 12);
  } catch (error) {
    const err = new Error("Could not create User. please try again.");
    err.code = 500;
    return next(err);
  }

  const createUser = new User({
    email,
    password: hashPass,
    firstname: firstName,
    lastname: lastName,
  });

  try {
    await createUser.save();
  } catch (errs) {
    const error = new Error("Creating user failed. Please try again!");
    error.code = 500;
    return next(error);
  }

  let token;
  try {
    token = jwt.sign(
      { userId: createUser.id, email: createUser.email },
      `${process.env.JWT_SECRET}`,
      { expiresIn: "1h" }
    );
  } catch (err) {
    const error = new Error("Creating user failed. Please try again!");
    error.code = 500;
    return next(error);
  }

  res.status(201).json({
    userId: createUser.id,
    email: createUser.email,
    firstname: createUser.firstname,
    lastname: createUser.lastname,
    token: token,
  });
};

exports.signin = signin;
exports.register = register;
