const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true, minlength: 6 },
  firstname: { type: String, required: false },
  lastname: { type: String, required: false },
});

module.exports = mongoose.model("User", userSchema);
